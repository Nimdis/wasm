(module
 (type $iv (func (param i32)))
 (type $v (func))
 (import "console" "log" (func $assembly/index/console.log (param i32)))
 (memory $0 1)
 (data (i32.const 8) "\0b\00\00\00H\00e\00l\00l\00o\00 \00w\00o\00r\00l\00d")
 (export "memory" (memory $0))
 (start $start)
 (func $start (; 1 ;) (; has Stack IR ;) (type $v)
  ;;@ assembly/index.ts:5:8
  (call $assembly/index/console.log
   ;;@ assembly/index.ts:5:12
   (i32.const 8)
  )
 )
)
